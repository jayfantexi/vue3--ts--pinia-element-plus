import request from "@/utils/request";

//品牌单独数据类型
export interface trademarkItemType {
    id?: number;
    tmName: string;
    logoUrl: string;
}
//品牌列表
export type trademarkListType = trademarkItemType[];
//品牌完整对象
export interface trademarkInfo {
    records: trademarkListType;
    total: number;
    size: number;
    current: number;
    searchCount: boolean;
    pages: number;
}
//请求分页列表
export const reqTrademarkList  = (page:number,limit:number)=>{
    return request.get<null,trademarkInfo>(`/admin/product/baseTrademark/${page}/${limit}`)
}
//2. 新增品牌
export const reqAddTrademark = (trademarkData: trademarkItemType) => {
    return request.post<null, null>(
        `/admin/product/baseTrademark/save`,
        trademarkData
    );
};

//3.修改品牌
export const reqUpdateTrademark = (trademarkData: trademarkItemType) => {
    return request.put<null, null>(
        `/admin/product/baseTrademark/update`,
        trademarkData
    );
};
//删除品牌
export const reqDeleteTrademark = (id: number) => {
    return request.delete<null,null>(`/admin/product/baseTrademark/remove/${id}`);
};
