import request from "@/utils/request";

export interface categoryInfoType {
    id:number;
    name:string;
    category1Id?: number;
    category2Id?: number;
}
//分类信息的列表类型
export type categoryListType = categoryInfoType[];

//1. 请求一级分类数据
export const reqCategory1List = () => {
    return request.get<null, categoryListType>(`/admin/product/getCategory1`);
};

//2. 请求二级分类数据
export const reqCategory2List = (category1Id: number) => {
    return request.get<null, categoryListType>(
        `/admin/product/getCategory2/${category1Id}`
    );
};

//3. 请求三级分类数据
export const reqCategory3List = (category2Id: number) => {
    return request.get<null, categoryListType>(
        `/admin/product/getCategory3/${category2Id}`
    );
};