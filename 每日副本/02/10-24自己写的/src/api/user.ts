import request from "@/utils/request";
//登录类型
export interface LoginParamType {
  username: string;
  password: string;
}
//登录以后类型
export interface LoginResultType {
  token: string;
}
export interface UserInfoType {
  routes: string[];
  buttons: string[];
  roles: string[];
  name: string;
  avatar: string;
}
//1. 登录请求
export const reqLogin = (userInfo: LoginParamType) => {
  return request.post<null, LoginResultType>(
    `/admin/acl/index/login`,
    userInfo
  );
};

//2.退出登录请求
export const reqLogout = () => {
  return request.post<null, null>(`/admin/acl/index/logout`);
};

//3.获取用户信息
export const reqUserInfo = () => {
  return request.get<null, UserInfoType>(`/admin/acl/index/info`);
};

//添加类型
export interface TrademarkDataType{
  id?:number;
  logoUrl:string;
  tmName:string;
}
export interface TrademarkListParamsType{
  page:number;
  limit:number;
}
export interface TrademarkListReturnType{
  records:TrademarkDataType[];
  total: number;
  size: number;
  current: number;
  searchCount: number;
  pages: number;
}

//获取信息
export const requestList = ({ page, limit }: TrademarkListParamsType) => {
  return request.get<any, TrademarkListReturnType>(`/admin/product/baseTrademark/${page}/${limit}`);
};
//添加
export const requestadd = (trademarkParam:TrademarkDataType)=>{
  return request.post<any,null>(`/admin/product/baseTrademark/save`,trademarkParam)
}
//更新
export const requestUpdate = (trademarkParam: TrademarkDataType) => {
  return request.put<any, null>(`/admin/product/baseTrademark/update`, trademarkParam);
};
//删除
export const requestRemove = (id:number)=>{
  return request.delete(`/admin/product/baseTrademark/remove/${id}`)
}


