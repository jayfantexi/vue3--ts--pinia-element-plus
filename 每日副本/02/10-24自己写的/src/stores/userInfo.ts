import { defineStore } from 'pinia';
import { getToken, removeToken, setToken } from '../utils/token-utils';
import type { UserInfoState } from './interface';
import {ElMessage} from 'element-plus'
import {staticRoutes} from '@/router/routes'
import {reqLogin ,reqUserInfo,reqLogout} from "@/api/user";


/**
 * 用户信息
 * @methods setUserInfos 设置用户信息
 */
export const useUserInfoStore = defineStore('userInfo', {

    state: (): UserInfoState => ({
        token: (getToken() as string) || "",
        userInfo: {
            routes: [],
            buttons: [],
            roles: [],
            name: "",
            avatar: "",
        },
        menuRoutes: staticRoutes
    }),

    actions: {
        //登录
        async login(username: string, password: string) {
            try {
                const result = await reqLogin({username, password})
                //本地存储
                setToken(result.token)
                //放入state中
                this.token = result.token
            } catch (e) {
                return Promise.reject(e)
            }
        },

        //获取个人信息
        async getInfo() {
            try {
                const result = await reqUserInfo()
                this.userInfo = result
            } catch (e) {
                return Promise.reject(e)
            }
        },
        //退出登录
        async logout() {
            try {
                await reqLogout()
                //调用reset，清除token，重置mutation
                this.reset();
            } catch (e) {
                return Promise.reject(e)
            }
        },
        reset() {
            // 删除local中保存的token
            removeToken()
            // 提交重置用户信息的mutation
            this.token = "",
                this.userInfo = {
                    routes: [],
                    buttons: [],
                    roles: [],
                    name: "",
                    avatar: "",
                };
        },
    },
})
