import request from "@/utils/request";

//1. 平台属性值的每一项的类型
export interface attrValueItemType {
    id?: number; //当前属性值的id
    valueName: string; //当前的属性值某个值
    attrId: number | undefined; //当前属性值所在的属性的id
    isEdit?: boolean;
}

//2.平台属性值列表类型
export type attrValueListType = attrValueItemType[];

//3.平台属性某一项的类型
export interface attrItemType {
    id?: number; //当前平台属性的id
    attrName: string; //当前平台属性的名字
    categoryId: number;
    categoryLevel: number;
    attrValueList: attrValueListType; //当前属性的属性值列表
}

//4.平台属性列表类型
export type attrListType = attrItemType[];

//1.根据3级分类请求当前分类的属性信息列表
export const reqAttrInfoList = (
    category1Id: number,
    category2Id: number,
    category3Id: number
) => {
    return request.get<null, attrListType>(
        `/admin/product/attrInfoList/${category1Id}/${category2Id}/${category3Id}`
    );
};
//2.提交新增属性或者修改属性
export const reqAddOrUpdateAttrList = (newAttr: attrItemType) => {
    return request.post(`/admin/product/saveAttrInfo`, newAttr);
};
//3. 删除某个属性的请求
export const reqDeleteAttr = (id: number) => {
    return request.delete(`/admin/product/deleteAttr/${id}`);
};