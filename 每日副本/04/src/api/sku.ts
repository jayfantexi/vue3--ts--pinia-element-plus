import request from "@/utils/request";

//1. sku列表的某一项公共类型
export interface skuInfoPublicType {
  id?: number;
  spuId: number; //所在的spuId
  price: number; //当前sku的价格
  skuName: string; //当前sku的名称
  skuDesc: string; //当前sku的描述
  weight: string; //当前sku的重量
  tmId: number; //当前sku所在的品牌id
  category3Id: number; //当前sku所在的三级分类的id
  skuDefaultImg: string; //当前sku的默认的图片
  isSale: number; //当前sku是否上架
  createTime: string; //当前sku的创建时间
}

//2. sku列表的某一项类型
export interface skuInfoItemType extends skuInfoPublicType {
  skuImageList: null; //当前sku图片列表
  skuAttrValueList: null; //当前sku属性值列表
  skuSaleAttrValueList: null; //当前sku销售属性值列表
}

//3.sku列表类型
export type skuInfoListType = skuInfoItemType[];

//4. sku图片列表的某一项类型
export interface skuImageItemType {
  id?: number;
  skuId: number; //所在的skuid
  imgName: string; //图片名称
  imgUrl: string; //图片地址
  spuImgId: number; //在spu中的id
  isDefault: string; //是否是当前的默认图片
}
//5. sku图片列表的类型
export type skuImageListType = skuImageItemType[];
//6. sku的平台属性值列表的某一项类型
export interface skuAttrValueItemType {
  id?: number;
  attrId: number; //当前平台属性值所在的平台属性在完整的平台属性列表中的id
  valueId: number; //当前平台属性值在当前属性的所有属性值中的id
  skuId: number; //当前的skuid
  attrName: string; //当前平台属性名
  valueName: string; //当前的平台属性值
}
//7.sku的平台属性值列表类型
export type skuAttrValueListType = skuAttrValueItemType[];

//8.sku的销售属性值列表的某一项的类型
export interface skuSaleAttrValueItemType {
  id?: number;
  skuId: number; //当前所在的skuId
  spuId: number; //当前sku所在的spuid
  saleAttrValueId: number; //当前销售属性值在他的完整销售属性值列表中的id
  saleAttrId: number; //当前销售属性在完整的销售属性列表中的id
  saleAttrName: string; //销售属性名称
  saleAttrValueName: string; //销售属性值的名称
}
//9.sku的销售属性值列表的类型
export type skuSaleAttrValueListType = skuSaleAttrValueItemType[];

//10. sku列表的详细类型
export interface skuInfoItemDetailType extends skuInfoPublicType {
  skuImageList: skuImageListType; //当前sku图片列表
  skuAttrValueList: skuAttrValueListType; //当前sku属性值列表
  skuSaleAttrValueList: skuSaleAttrValueListType; //当前sku销售属性值列表
}

//11. sku完整列表的信息类型
export interface allSkuInfoDetailType {
  records: skuInfoListType;
  total: number;
  size: number;
  current: number;
  searchCount: boolean;
  pages: number;
}

//1.sku上架
export const reqOnSale = (skuId: number) => {
  return request.get<null, null>(`/admin/product/onSale/${skuId}`);
};

//2.sku下架
export const reqCancelSale = (skuId: number) => {
  return request.get<null, null>(`/admin/product/cancelSale/${skuId}`);
};

//3. 删除某个sku
export const reqDeleteSku = (skuId: number) => {
  return request.delete<null, null>(`/admin/product/deleteSku/${skuId}`);
};

//4. 根据某个spuid查询他的所有sku列表(但是sku的信息不是详细的)
export const reqSkuListBySpuId = (spuId: number) => {
  return request.get<null, skuInfoListType>(
    `/admin/product/findBySpuId/${spuId}`
  );
};

//5. 根据skuid查询他的详细信息
export const reqSkuDetail = (skuId: number) => {
  return request.get<null, skuInfoItemDetailType>(
    `/admin/product/getSkuById/${skuId}`
  );
};

//6. 直接查询所有的sku列表
export const reqAllSkuList = (page: number, limit: number) => {
  return request.get<null, allSkuInfoDetailType>(
    `/admin/product/list/${page}/${limit}`
  );
};

//7. 新增sku
export const reqAddSku = (skuInfo:skuInfoItemDetailType) => {
  return request.post<null, null>(`/admin/product/saveSkuInfo`, skuInfo);
};
