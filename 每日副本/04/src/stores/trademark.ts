import {defineStore} from "pinia";
import {reqTrademarkList} from "@/api/trademark";

//引入品牌列表的数组的类型
import type { trademarkListType } from "@/api/trademark";

//定义当前store的state类型
export interface tmStoreStateType {
    trademarkList: trademarkListType;
    pages: number;
}
const useTrademarkStore  = defineStore("trademark",{
    state:():tmStoreStateType=>{
        return{
            trademarkList:[],
            pages:0
        }
    },
    actions:{
        getTrademake: async function (page: number, limit: number) {
            try {
                const re = await reqTrademarkList(page, limit)
                this.trademarkList = re.records;
                this.pages = re.pages
            } catch (e) {
                return Promise.reject(e)
            }
        }
    }
})
export default useTrademarkStore;