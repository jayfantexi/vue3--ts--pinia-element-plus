import request from "@/utils/request";
//登录类型
export interface LoginParamType {
  username: string;
  password: string;
}
//登录以后类型
export interface LoginResultType {
  token: string;
}
export interface UserInfoType {
  routes: string[];
  buttons: string[];
  roles: string[];
  name: string;
  avatar: string;
}
//1. 登录请求
export const reqLogin = (userInfo: LoginParamType) => {
  return request.post<null, LoginResultType>(
    `/admin/acl/index/login`,
    userInfo
  );
};

//2.退出登录请求
export const reqLogout = () => {
  return request.post<null, null>(`/admin/acl/index/logout`);
};

//3.获取用户信息
export const reqUserInfo = () => {
  return request.get<null, UserInfoType>(`/admin/acl/index/info`);
};
