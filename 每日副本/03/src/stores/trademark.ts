import {defineStore} from "pinia";
import {reqTrademarkList} from "@/api/trademark";
import type {trademarkListType} from "@/api/trademark";

//定义当前store的state类型
export interface tmStoreStateType{
    trademarkList:trademarkListType;
    pages:number
}
const useTrademarkStore = defineStore("trademark",{
    state:():tmStoreStateType=>{
        return{
          //品牌列表
          trademarkList:[],
          //品牌列表总页数
          pages:0
        }
    },
    actions:{
        //获取品牌分页列表
        async getTrademark(page:number,limit:number){
            try {
                const re = await reqTrademarkList(page,limit)
                this.trademarkList = re.records;
                this.pages = re.pages
            }catch (e){
                return Promise.reject(e)
            }
        }
}
})
export default useTrademarkStore