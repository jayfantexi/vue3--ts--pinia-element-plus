import {defineStore} from "pinia";
import type {categoryListType} from "@/api/category";
import {
    reqCategory1List,
    reqCategory2List,
    reqCategory3List
} from "@/api/category";

//定义store类型
export interface categoryStateType {
    category1List: categoryListType;
    category2List: categoryListType;
    category3List: categoryListType;
    category1Id: null | number;
    category2Id: null | number;
    category3Id: null | number;
}

//store的定义
export const useCategoryStore = defineStore("category", {
    state: (): categoryStateType => {
        return {
            //三级列表
            category1List: [],
            category2List: [],
            category3List: [],
            //列表id
            category1Id: null,
            category2Id: null,
            category3Id: null,
        }
    },
    actions: {
        //请求一级分类
        async getCategory1List() {
            try {
                const re = await reqCategory1List();
                this.category1List = re
            } catch (e: any) {
                return Promise.reject(e)
            }
        },
        //请求二级分类
        async getCategory2List() {
            try {
                const re = await reqCategory2List(this.category1Id as number);
                this.category2List = re
            } catch (e: any) {
                return Promise.reject(e)
            }
        },
        //请求三级分类
        async getCategory3List() {
            try {
                const re = await reqCategory3List(this.category2Id as number);
                this.category3List = re
            } catch (e: any) {
                return Promise.reject(e)
            }
        }
    }
})
export default useCategoryStore