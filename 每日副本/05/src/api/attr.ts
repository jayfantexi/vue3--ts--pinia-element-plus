import request from "@/utils/request";

//每一项类型
export interface attrValueItemType {
    id?: number,
    valueName: string,
    attrId: number | undefined,
    isEdit?: boolean;
}

//列表类型
export type attrValueListType = attrValueItemType[];

export interface attrItemType {
    id?: number,
    attrName: string,
    categoryId: number;
    categoryLevel: number;
    attrValueList: attrValueListType; //当前属性的属性值列表
}

export type attrListType = attrItemType[]

//1.根据3级分类请求当前分类的属性信息列表
export const reqAttrInfoList = (category1Id: number, category2Id: number, category3Id: number) => {
    return request.get<null, attrListType>(
        `/admin/product/attrInfoList/${category1Id}/${category2Id}/${category3Id}`
    );
};
//2.提交新增属性或者修改属性
export const reqAddOrUpdateAttrList = (newAttr: attrItemType) => {
    return request.post(`/admin/product/saveAttrInfo`, newAttr);
};
//3. 删除某个属性的请求
export const reqDeleteAttr = (id: number) => {
    return request.delete(`/admin/product/deleteAttr/${id}`);
};
