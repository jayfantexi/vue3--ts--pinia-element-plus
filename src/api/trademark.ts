import request from "@/utils/request";

//1. 品牌列表中某一个品牌数据对象的类型
export interface trademarkItemType {
    id?: number;
    tmName: string;
    logoUrl: string;
}

//2. 品牌列表数组的类型
export type trademarkListType = trademarkItemType[];


//3. 品牌分页列表完整的对象数据类型
export interface trademarkInfo {
    records: trademarkListType;
    total: number;
    size: number;
    current: number;
    searchCount: boolean;
    pages: number;
}

//1. 请求品牌分页列表
export const reqTrademarkList = (page: number, limit: number) => {
    return request.get<null, trademarkInfo>(
        `/admin/product/baseTrademark/${page}/${limit}`
    );
};
//2. 新增品牌
export const reqAddTrademark = (trademarkData: trademarkItemType) => {
    return request.post<null, null>(
        `/admin/product/baseTrademark/save`,
        trademarkData
    );
};

//3.修改品牌
export const reqUpdateTrademark = (trademarkData: trademarkItemType) => {
    return request.put<null, null>(
        `/admin/product/baseTrademark/update`,
        trademarkData
    );
};
//4. 删除某个品牌
export const reqDeleteTrademark = (id: number) => {
    return request.delete<null, null>(
        `/admin/product/baseTrademark/remove/${id}`
    );
};
//5. 请求所有品牌列表
export const reqAllTrademark = () => {
    return request.get<null, trademarkListType>(
        `/admin/product/baseTrademark/getTrademarkList`
    );
};
