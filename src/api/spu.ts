import request from "@/utils/request";

//1.基础销售属性单个的类型
export interface baseSaleAttrItemType {
    id?: number;
    name: string;
}

//2. 基础销售属性列表的类型
export type baseSaleAttrListType = baseSaleAttrItemType[];

//3. 某一个spu对象的类型
export interface spuItemType {
    id?: number;
    spuName: string;
    description: string;
    category3Id: number;
    tmId: number;
    //在分页列表的请求中,当前spu的拥有的销售属性是null,因为不需要展示
    spuSaleAttrList: null | saleAttrListType;
    //在分页列表的请求中,当前spu的拥有的图片列表是null,因为不需要展示
    spuImageList: null | spuListType;
}

//4.spu分页列表的数组类型
export type spuListType = spuItemType[];

//5.spu分页列表的返回值类型
export interface spuListReturnType {
    records: spuListType;
    total: number;
    size: number;
    current: number;
    searchCount: boolean;
    pages: number;
}

//6.某个spu的图片列表某一个类型
export interface spuImgItemType {
    id?: number;
    imgName: string;
    imgUrl: string;
    spuId?: number; //当前图片所在的spu的id
    name?: string;
    url?: string;
    response?: any;
    isDefault?: string;
}

//7.某个spu的图片列表类型
export type spuImgListType = spuImgItemType[];

//8.销售属性值列表的某一个项的类型
export interface saleAttrValueItemType {
    baseSaleAttrId: number; //当前销售属性值所在的销售属性的id
    id?: number;
    isChecked: string; //当前销售属性值是否是被选中状态
    saleAttrName: string; //所在的销售属性的名称
    saleAttrValueName: string; //所在的销售属性值的名称
    spuId?: number; //当前销售属性所在的spu的id
}

//9.销售属性值列表的类型
export type saleAttrValueListType = saleAttrValueItemType[];

//10.销售属性列表的某一项的类型
export interface saleAttrItemType {
    baseSaleAttrId: number; //当前销售属性在基础销售属性列表中的id
    id?: number;
    saleAttrName: string; //当前销售属性的名称
    spuId?: number; //当前销售属性所在的spuId
    spuSaleAttrValueList: saleAttrValueListType; //当前销售属性的销售属性值列表
    isEdit?: boolean;
    selectData?: string;
}

//11.销售属性列表的类型
export type saleAttrListType = saleAttrItemType[];

//12.新增或修改的参数类型
export interface spuInfoType {
    category3Id: number; //所在的第三级分类id
    description: string; //当前新增的spu的描述
    id?: number;
    spuImageList: spuImgListType; //当前spu的图片列表
    spuName: string; //当前的spu名称
    spuSaleAttrList: saleAttrListType; //当前spu的销售属性列表
    tmId: number; //当前所在的品牌id
}

//1.请求所有的销售属性组成的列表
export const reqBaseSaleAttrList = () => {
    return request.get<null, baseSaleAttrListType>(
        `/admin/product/baseSaleAttrList`
    );
};

//2.删除某一个spu
export const reqDeleteSpu = (spuId: number) => {
    return request.delete<null, null>(`/admin/product/deleteSpu/${spuId}`);
};

//3.获取spu的分页列表
export const reqSpuList = (
    page: number,
    limit: number,
    category3Id: number
) => {
    return request.get<null, spuListReturnType>(
        `/admin/product/${page}/${limit}?category3Id=${category3Id}`
    );
};

//4. 新增新的spu接口
export const reqAddSpu = (spuInfo: spuItemType) => {
    return request.post<null, null>(`/admin/product/saveSpuInfo`, spuInfo);
};

//5. 修改spu的接口
export const reqUpdateSpu = (spuInfo: spuItemType) => {
    return request.post<null, null>(`/admin/product/updateSpuInfo`, spuInfo);
};

//6. 根据spuId获取自己的销售属性列表
export const reqSaleAttrSelf = (spuId: number) => {
    return request.get<null, saleAttrListType>(
        `/admin/product/spuSaleAttrList/${spuId}`
    );
};

//7. 根据spuId获取ImgList列表
export const reqSpuImageList = (spuId: number) => {
    return request.get<null, spuImgListType>(
        `/admin/product/spuImageList/${spuId}`
    );
};